﻿using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    class OrderController
    {
        public List<Order> GetAll()
        {
            List<Order> orders = new List<Order>();
            string sql = "select * from orders";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                Order order = new Order(row);
                orders.Add(order);
            }    
            return orders;
        }
        public bool Add(Order order)
        {
            string sql = @"insert into orders (CustomerId,StoreID,commodityName,weigh,receiveDate,shipDate,shipAddress,status,unitPrice,distance) values
						( @customerId , @StoreId , @commodityName , @weigh , @receiveDate , @shipDate , @shipAddress , @status , @unitPrice , @distance )";
            MyConnection.ExecuteNonQuery(sql, new object[] { 
                order.CustomerId,
                order.StoreID,
                order.commodityName,
                order.weigh,
                order.receiveDate,
                order.shipDate,
                order.shipAddress,
                order.status,
                order.unitPrice,
                order.distance,
            });
            return true;
        }
        public bool Edit(Order order)
        {
            string sql = @"update orders set CustomerId = @CustomerId ,StoreID = @StoreID ,commodityName = @commodityName ,weigh = @weigh ,receiveDate = @receiveDate ,shipDate = @shipDate ,shipAddress = @shipAddress ,status = @status ,unitPrice = @unitPrice ,distance = @distance where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] {
                order.CustomerId,
                order.StoreID,
                order.commodityName,
                order.weigh,
                order.receiveDate,
                order.shipDate,
                order.shipAddress,
                order.status,
                order.unitPrice,
                order.distance,
                order.id
            }) ;
            return true;
        }
    }
}
