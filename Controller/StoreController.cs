﻿using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    class StoreController
    {
        public List<Store> GetAll()
        {
            List<Store> stores = new List<Store>();
            String sql = "select * from store";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach (DataRow row in table.Rows)
            {
                Store store = new Store(row);
                stores.Add(store);
            }
            return stores;
        }
        public bool Add(Store store)
        {
            string sql = "insert into store (name, area, Storeaddress) values ( @name , @area , @storeAddress )";
            MyConnection.ExecuteNonQuery(sql, new object[] { store.name, store.area, store.Storeaddress });
            return true;
        }
        public bool Delete(int id)
        {
            String sql = "delete from store where id = @id";
            MyConnection.ExecuteNonQuery(sql, new Object[] { id });
            return true;
        }
        public bool Edit(Store store)
        {
            string sql = "update store set name = @name , area = @area , Storeaddress = @Storeaddress where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { store.name, store.area, store.Storeaddress,store.id });
            return true;
        }
        public Store GetById(int id)
        {
            string sql = "select * from store where id = @id";
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if(table.Rows.Count==0)
            {
                return null;
            }
            return new Store(table.Rows[0]);
        }
    }
}
