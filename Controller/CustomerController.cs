﻿using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    class CustomerController
    {
        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "select * from customer";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach (DataRow row in table.Rows)
            {
                Customer customer = new Customer(row);
                customers.Add(customer);
            }
            return customers;
        }
        public bool Add(Customer customer)
        {
            string sql = "insert into customer (name, address, phone) values( @name , @address , @phone )";
            MyConnection.ExecuteNonQuery(sql, new object[] { customer.name, customer.address, customer.phone });
            return true;
        }
        public bool Delete(int id)
        {
            string sql = "delete from customer where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { id });
            return true;
        }
        public bool Edit(Customer customer)
        {
            string sql = "update customer set name = @name , address = @address , phone = @phone where id = @id";
            MyConnection.ExecuteNonQuery(sql, new object[] { customer.name, customer.address, customer.phone, customer.id });
            return true;
        }
        public Customer GetByPhone(string phone)
        {
            string sql = "select * from customer where phone = @phone";
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { phone });
            if(table.Rows.Count==0)
            {
                return null;
            }
            Customer customer = new Customer(table.Rows[0]);
            return customer;
        }
        public Customer GetById(string id)
        {
            string sql = "select * from customer where id = @phone";
            DataTable table = MyConnection.ExecuteQuery(sql, new object[] { id });
            if (table.Rows.Count == 0)
            {
                return null;
            }
            Customer customer = new Customer(table.Rows[0]);
            return customer;
        }
    }
}
