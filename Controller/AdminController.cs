﻿using QuanLyVanTai.Model;
using QuanLyVanTai.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    class AdminController
    {
        public Admin GetById(int id)
        {
            String query = "select * from admin where id = @id";
            DataRow admin = MyConnection.ExecuteQuery(query, new object[] { id }).Rows[0];
            return new Admin(admin);
        }
        public bool LogIn(string username, string pasword)
        {
            string hashPass = PasswordController.Sha1Hash(pasword);
            String query = "select * from admin where usename = @username and hashpass = @hashpass";
            DataTable admins = MyConnection.ExecuteQuery(query, new object[] { username, hashPass });
            if(admins.Rows.Count==0)
            {
                return false;
            }    
            return true;
        }
        public bool Add(Admin admin)
        {
            string sql = @"insert into admin (usename,hashpass)values ( @admin , @hashpass )";
            MyConnection.ExecuteNonQuery(sql, new object[] { admin.usename, admin.hashpass });
            return true;
        }
    }
}
