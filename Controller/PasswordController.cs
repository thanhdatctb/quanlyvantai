﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    static class PasswordController
    {
        //Hàm mã hóa pasword theo thuật toán SHA-1
        public static string Sha1Hash(String password)
        {
            var data = Encoding.ASCII.GetBytes(password);
            var hashData = new SHA1Managed().ComputeHash(data);
            var hash = string.Empty;
            foreach (var b in hashData)
            {
                hash += b.ToString("X2");
            }
            return hash;
            //var md5 = new MD5CryptoServiceProvider();
            //var data = Encoding.ASCII.GetBytes(password);
            //var md5data = md5.ComputeHash(data);
            //var hashedPassword = ASCIIEncoding.GetString(md5data);
            //Console.WriteLine(Encoding.ASCII.GetString(md5data));
            //return Encoding.ASCII.GetString(md5data); 
        }

        //Hàm mã hóa password theo thuật toán md5
        public static string MD5Hash(string password) 
        {
            var data = Encoding.ASCII.GetBytes(password);
            var hashData = new MD5CryptoServiceProvider().ComputeHash(data); 

            //Đoạn này tương tự hàm SHA1Hash ở phía trên
            var hash = string.Empty;
            foreach (var b in hashData)
            {
                hash += b.ToString("X2");
            }
            return hash;
        }
    }
}
