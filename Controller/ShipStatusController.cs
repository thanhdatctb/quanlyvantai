﻿using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Controller
{
    class ShipStatusController
    {
        public List<ShipStatus> GetAll()
        {
            List<ShipStatus> shipStatuses = new List<ShipStatus>();
            string sql = "select * from shipstatus";
            DataTable table = MyConnection.ExecuteQuery(sql);
            foreach(DataRow row in table.Rows)
            {
                ShipStatus shipStatus = new ShipStatus(row);
                shipStatuses.Add(shipStatus);
            }    
            return shipStatuses;
        }
    }
}
