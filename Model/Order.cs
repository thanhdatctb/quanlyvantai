﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Model
{
    public class Order
    {
        public int id { get; set; }
        public int CustomerId { get; set; }
        public int StoreID { get; set; }
        public string commodityName { get; set; }
        public float weigh { get; set; }
        public DateTime receiveDate { get; set; }
        public DateTime shipDate { get; set; }
        public string shipAddress { get; set; }
        public string status { get; set; }
        public float unitPrice { get; set; }
        public float distance { get; set; }

        public Order(int id, int customerId, int storeID, string commodityName, float weigh, DateTime receiveDate, DateTime shipDate, string shipAddress, string status, float unitPrice, float distance)
        {
            this.id = id;
            CustomerId = customerId;
            StoreID = storeID;
            this.commodityName = commodityName;
            this.weigh = weigh;
            this.receiveDate = receiveDate;
            this.shipDate = shipDate;
            this.shipAddress = shipAddress;
            this.status = status;
            this.unitPrice = unitPrice;
            this.distance = distance;
        }

        public Order(int customerId, int storeID, string commodityName, float weigh, DateTime receiveDate, DateTime shipDate, string shipAddress, string status, float unitPrice, float distance)
        {
            CustomerId = customerId;
            StoreID = storeID;
            this.commodityName = commodityName;
            this.weigh = weigh;
            this.receiveDate = receiveDate;
            this.shipDate = shipDate;
            this.shipAddress = shipAddress;
            this.status = status;
            this.unitPrice = unitPrice;
            this.distance = distance;
        }

        public Order()
        {
        }
        public Order(DataRow row)
        {
            this.id = (int)row["id"];
            this.CustomerId = (int)row["CustomerId"];
            this.StoreID = (int)row["StoreID"];
            this.commodityName = (string)row["commodityName"];
            this.weigh = float.Parse(row["weigh"].ToString());
            this.receiveDate = (DateTime)row["receiveDate"];
            this.shipDate = (DateTime)row["shipDate"];
            this.shipAddress = (string)row["shipAddress"];
            this.status = (string)row["status"];
            this.unitPrice = float.Parse(row["unitPrice"].ToString());
            this.distance = float.Parse(row["distance"].ToString());
        }
    }

}
