﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Model
{
    class Store
    {
        public int id { get; set; }
        public string name { get; set; }
        public float area { get; set; }
        public string Storeaddress { get; set; }

        public Store(int id, string name, float area, string storeaddress)
        {
            this.id = id;
            this.name = name;
            this.area = area;
            Storeaddress = storeaddress;
        }

        public Store(string name, float area, string storeaddress)
        {
            this.name = name;
            this.area = area;
            Storeaddress = storeaddress;
        }

        public Store()
        {
        }
        public Store(DataRow row)
        {
            this.id = (int)row["id"];
            this.name = row["name"].ToString();
            this.area = float.Parse(row["area"].ToString());
            this.Storeaddress = row["Storeaddress"].ToString();
        }
    }
}
