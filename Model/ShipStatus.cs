﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Model
{
    class ShipStatus
    {
        public int Id { get; set; }
        public string name { get; set; }
        public ShipStatus(DataRow row)
        {
            this.Id = (int)row["id"];
            this.name = row["name"].ToString();
        }
    }
}
