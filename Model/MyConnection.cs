﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Model
{
    static class MyConnection
    {
        private static string connectionString;
        static SqlConnection conn = GetConnection();

        public static SqlConnection GetConnection()
        {
            if (conn == null)
            {
                connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                conn = new SqlConnection(connectionString);
            }
            return conn;
        }
        public static SqlCommand GetCommand(string sql)
        { 
            return new SqlCommand(sql, conn);
        }

        public static SqlDataReader ExcuteQueryWithId(string table, int id)
        {
            
            conn.Open();
            String sql = "select * from " + table + " where id = @id";
            SqlCommand cmd = GetCommand(sql);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            SqlDataReader result = null;
            while (reader.Read())
            {
                result = reader;
                break;
            }

            return result;
        }



        public static DataTable ExecuteQuery(string query, object[] parameter = null)
        {
            DataTable data = new DataTable();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' '); 
                    int i = 0;


                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(data);

                connection.Close();

            }

            return data;
        }
        public static int ExecuteNonQuery(string query, object[] parameter = null)
        {
           
            int data = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }

                data = command.ExecuteNonQuery();

                connection.Close();

            }

            return data;
        }

        public static object ExcuteScalar(string query, object[] parameter = null)
        {

            object data = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }
                data = command.ExecuteScalar();

                connection.Close();

            }

            return data;
        }
        public static void ExecuteNonQuery(SqlCommand cmd)
        {

        }
    }
}
