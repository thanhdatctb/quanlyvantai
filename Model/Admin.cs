﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyVanTai.Model
{
    class Admin
    {
        public int id { get; set; }
        public string usename { get; set; }
        public string hashpass { get; set; }

        public Admin(int id, string usename, string hashpass)
        {
            this.id = id;
            this.usename = usename;
            this.hashpass = hashpass;
        }

        public Admin(string usename, string hashpass)
        {
            this.usename = usename;
            this.hashpass = hashpass;
        }

        public Admin()
        {
        }
        public Admin(DataRow row)
        {
            this.id = (int)row["id"];
            this.usename = row["usename"].ToString();
            this.hashpass = row["hashpass"].ToString();
        }
    }
}
