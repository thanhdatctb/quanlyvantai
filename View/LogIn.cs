﻿using QuanLyVanTai.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class LogIn : Form
    {
        AdminController adminController = new AdminController();
        public LogIn()
        {
            InitializeComponent();
        }

        
        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string username = this.txtUsername.Text;
            string password = this.txtPassword.Text;
            if(adminController.LogIn(username,password))
            {
                new FormHome().Show();
            }
            else
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
            }    
        }

        private void label5_Click(object sender, EventArgs e)
        {
            new FormSignUp().Show();
        }
    }
}
