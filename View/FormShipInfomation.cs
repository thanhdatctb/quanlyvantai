﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class FormShipInfomation : Form
    {
        private OrderController orderController = new OrderController();
        private StoreController storeController = new StoreController();
        private CustomerController customerController = new CustomerController();
        private ShipStatusController shipStatusController = new ShipStatusController();
        public FormShipInfomation()
        {
            InitializeComponent();
            LoadData();
            this.cbStore.DataSource = storeController.GetAll();
            this.cbStore.DisplayMember = "name";
            this.cbStore.ValueMember = "id";
            this.cbStatus.DataSource = shipStatusController.GetAll();
            this.cbStatus.DisplayMember = "name";
            this.cbStatus.ValueMember = "name";

            this.txtId.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "id"));
            this.txtCustomerId.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "customerId"));
            this.cbStore.DataBindings.Add(new Binding("SelectedValue", dtgvOrder.DataSource, "id"));
            this.txtComodityName.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "commodityName"));
            this.txtWeight.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "weigh"));
            this.dpDReceiveDate.DataBindings.Add(new Binding("Value", dtgvOrder.DataSource, "receiveDate"));
            this.dpShipDate.DataBindings.Add(new Binding("Value", dtgvOrder.DataSource, "shipDate"));
            this.txtShipAddress.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "shipAddress"));
            this.txtUnitPrice.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "unitprice"));
            this.txtDistance.DataBindings.Add(new Binding("Text", dtgvOrder.DataSource, "distance"));
            this.cbStatus.DataBindings.Add(new Binding("SelectedValue", dtgvOrder.DataSource, "status"));
            
        }
        private void LoadData()
        {
            this.dtgvOrder.DataSource = this.orderController.GetAll();
        }
        private void dtgvOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int CustomerId = int.Parse(this.txtCustomerId.Text);
            int StoreID = int.Parse(this.cbStore.SelectedValue.ToString());
            string commodityName = this.txtComodityName.Text;
            float weigh = float.Parse(this.txtWeight.Text);
            DateTime receiveDate = dpDReceiveDate.Value;
            DateTime shipDate = dpShipDate.Value;
            string shipAddress = this.txtShipAddress.Text;
            string status = cbStatus.SelectedValue.ToString();
            float unitPrice = float.Parse(this.txtUnitPrice.Text);
            float distance = float.Parse(this.txtDistance.Text);
            int id = int.Parse(this.txtId.Text);
            Order order = new Order(id,CustomerId, StoreID, commodityName, weigh, receiveDate, shipDate, shipAddress, status, unitPrice, distance);
            try
            {
                if(orderController.Edit(order))
                {
                    LoadData();
                    MessageBox.Show("Update thành công");
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void txtCustomerId_TextChanged(object sender, EventArgs e)
        {
            Customer customer = customerController.GetById(this.txtCustomerId.Text);
            this.txtName.Text = customer.name;
            this.txtAddress.Text = customer.address;
            this.txtPhone.Text = customer.phone;
        }

        private void btnExportBill_Click(object sender, EventArgs e)
        {
            int CustomerId = int.Parse(this.txtCustomerId.Text);
            int StoreID = int.Parse(this.cbStore.SelectedValue.ToString());
            string commodityName = this.txtComodityName.Text;
            float weigh = float.Parse(this.txtWeight.Text);
            DateTime receiveDate = dpDReceiveDate.Value;
            DateTime shipDate = dpShipDate.Value;
            string shipAddress = this.txtShipAddress.Text;
            string status = cbStatus.SelectedValue.ToString();
            float unitPrice = float.Parse(this.txtUnitPrice.Text);
            float distance = float.Parse(this.txtDistance.Text);
            int id = int.Parse(this.txtId.Text);
            Order order = new Order(id, CustomerId, StoreID, commodityName, weigh, receiveDate, shipDate, shipAddress, status, unitPrice, distance);
            new FormBill(order).Show();
        }
    }
}
