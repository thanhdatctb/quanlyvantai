﻿namespace QuanLyVanTai.View
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hệThốngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripDK = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripDX = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuItemStore = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripNCC = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripVT = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripNK = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngTinVậnChuyểnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoThốngKêToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuToolTripRevenue = new System.Windows.Forms.ToolStripMenuItem();
            this.DT = new System.Windows.Forms.ToolStripMenuItem();
            this.panelWorkMain = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hệThốngToolStripMenuItem,
            this.StripMenuItemStore,
            this.nhậpXuấtToolStripMenuItem,
            this.báoCáoThốngKêToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(875, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hệThốngToolStripMenuItem
            // 
            this.hệThốngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuToolTripDK,
            this.MenuToolTripDX});
            this.hệThốngToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hệThốngToolStripMenuItem.Name = "hệThốngToolStripMenuItem";
            this.hệThốngToolStripMenuItem.Size = new System.Drawing.Size(74, 21);
            this.hệThốngToolStripMenuItem.Text = "Hệ thống";
            // 
            // MenuToolTripDK
            // 
            this.MenuToolTripDK.Name = "MenuToolTripDK";
            this.MenuToolTripDK.Size = new System.Drawing.Size(137, 22);
            this.MenuToolTripDK.Text = "Đăng Ký";
            this.MenuToolTripDK.Click += new System.EventHandler(this.MenuToolTripDK_Click);
            // 
            // MenuToolTripDX
            // 
            this.MenuToolTripDX.Name = "MenuToolTripDX";
            this.MenuToolTripDX.Size = new System.Drawing.Size(137, 22);
            this.MenuToolTripDX.Text = "Đăng Xuất";
            this.MenuToolTripDX.Click += new System.EventHandler(this.MenuToolTripDX_Click);
            // 
            // StripMenuItemStore
            // 
            this.StripMenuItemStore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuToolTripNCC,
            this.MenuToolTripVT});
            this.StripMenuItemStore.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StripMenuItemStore.Name = "StripMenuItemStore";
            this.StripMenuItemStore.Size = new System.Drawing.Size(78, 21);
            this.StripMenuItemStore.Text = "Danh mục";
            // 
            // MenuToolTripNCC
            // 
            this.MenuToolTripNCC.Name = "MenuToolTripNCC";
            this.MenuToolTripNCC.Size = new System.Drawing.Size(144, 22);
            this.MenuToolTripNCC.Text = "Kho bãi";
            this.MenuToolTripNCC.Click += new System.EventHandler(this.MenuToolTripNCC_Click);
            // 
            // MenuToolTripVT
            // 
            this.MenuToolTripVT.Name = "MenuToolTripVT";
            this.MenuToolTripVT.Size = new System.Drawing.Size(144, 22);
            this.MenuToolTripVT.Text = "Khách hàng";
            this.MenuToolTripVT.Click += new System.EventHandler(this.MenuToolTripVT_Click);
            // 
            // nhậpXuấtToolStripMenuItem
            // 
            this.nhậpXuấtToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuToolTripNK,
            this.thôngTinVậnChuyểnToolStripMenuItem});
            this.nhậpXuấtToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nhậpXuấtToolStripMenuItem.Name = "nhậpXuấtToolStripMenuItem";
            this.nhậpXuấtToolStripMenuItem.Size = new System.Drawing.Size(83, 21);
            this.nhậpXuấtToolStripMenuItem.Text = "Nhập-Xuất";
            // 
            // MenuToolTripNK
            // 
            this.MenuToolTripNK.Name = "MenuToolTripNK";
            this.MenuToolTripNK.Size = new System.Drawing.Size(199, 22);
            this.MenuToolTripNK.Text = "Nhập kho";
            this.MenuToolTripNK.Click += new System.EventHandler(this.MenuToolTripNK_Click);
            // 
            // thôngTinVậnChuyểnToolStripMenuItem
            // 
            this.thôngTinVậnChuyểnToolStripMenuItem.Name = "thôngTinVậnChuyểnToolStripMenuItem";
            this.thôngTinVậnChuyểnToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.thôngTinVậnChuyểnToolStripMenuItem.Text = "Thông tin vận chuyển";
            this.thôngTinVậnChuyểnToolStripMenuItem.Click += new System.EventHandler(this.thôngTinVậnChuyểnToolStripMenuItem_Click);
            // 
            // báoCáoThốngKêToolStripMenuItem
            // 
            this.báoCáoThốngKêToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuToolTripRevenue,
            this.DT});
            this.báoCáoThốngKêToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.báoCáoThốngKêToolStripMenuItem.Name = "báoCáoThốngKêToolStripMenuItem";
            this.báoCáoThốngKêToolStripMenuItem.Size = new System.Drawing.Size(134, 21);
            this.báoCáoThốngKêToolStripMenuItem.Text = "Báo cáo - Thống kê";
            // 
            // MenuToolTripRevenue
            // 
            this.MenuToolTripRevenue.Name = "MenuToolTripRevenue";
            this.MenuToolTripRevenue.Size = new System.Drawing.Size(217, 22);
            this.MenuToolTripRevenue.Text = "Thống kê doanh thu";
            this.MenuToolTripRevenue.Click += new System.EventHandler(this.MenuToolTripRevenue_Click);
            // 
            // DT
            // 
            this.DT.Name = "DT";
            this.DT.Size = new System.Drawing.Size(217, 22);
            this.DT.Text = "Xuất báo cáo doanh thu";
            this.DT.Click += new System.EventHandler(this.DT_Click);
            // 
            // panelWorkMain
            // 
            this.panelWorkMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelWorkMain.BackgroundImage = global::QuanLyVanTai.Properties.Resources._9f5d6b94496517_5e808d30f29c2;
            this.panelWorkMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelWorkMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWorkMain.Location = new System.Drawing.Point(0, 25);
            this.panelWorkMain.Name = "panelWorkMain";
            this.panelWorkMain.Size = new System.Drawing.Size(875, 525);
            this.panelWorkMain.TabIndex = 4;
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 550);
            this.Controls.Add(this.panelWorkMain);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormHome";
            this.Text = "FormHome";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelWorkMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hệThốngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripDK;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripDX;
        private System.Windows.Forms.ToolStripMenuItem StripMenuItemStore;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripNCC;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripVT;
        private System.Windows.Forms.ToolStripMenuItem nhậpXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripNK;
        private System.Windows.Forms.ToolStripMenuItem báoCáoThốngKêToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuToolTripRevenue;
        private System.Windows.Forms.ToolStripMenuItem DT;
        private System.Windows.Forms.ToolStripMenuItem thôngTinVậnChuyểnToolStripMenuItem;
    }
}