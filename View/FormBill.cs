﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class FormBill : Form
    {
        private CustomerController customerController = new CustomerController();
        private StoreController storeController = new StoreController();
        public FormBill(Order order)
        {
            InitializeComponent();
            Customer customer = customerController.GetById(order.CustomerId.ToString());
            Store store = storeController.GetById(order.StoreID);
            this.txtCustomerName.Text = customer.name;
            this.txtAddress.Text = customer.address;
            this.txtPhone.Text = customer.phone;

            this.txtStoreName.Text = store.name;
            this.txtStoreAddress.Text = store.Storeaddress;

            this.txtCommodityname.Text = order.commodityName;
            this.txtReceiveDate.Text = order.receiveDate.ToString();
            this.txtShipDate.Text = order.shipDate.ToString();
            this.txtWeight.Text = order.weigh.ToString() + " Tấn";
            this.txtShipAddress.Text = order.shipAddress;
            this.txtDistance.Text = order.distance.ToString() +" km";
            this.txtUnitPrice.Text = order.unitPrice.ToString() +" VNĐ";
            this.txtStatus.Text = order.status;

            float total = order.distance * order.unitPrice * order.weigh;
            this.txtTotal.Text = total.ToString() + " VND";
        }
    }
}
