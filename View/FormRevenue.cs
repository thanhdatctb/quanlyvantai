﻿using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class FormRevenue : Form
    {
        private DataTable GetData()
        {
            string sql = "select receiveDate as [Ngày], SUM(weigh* unitPrice * distance) as [Tổng doanh thu] from orders group by receiveDate";
            return MyConnection.ExecuteQuery(sql);
        }
        public FormRevenue()
        {
            InitializeComponent();
            this.dtgvRevenue.DataSource = GetData();
        }
    }
}
