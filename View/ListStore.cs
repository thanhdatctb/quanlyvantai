﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class ListStore : Form
    {
        private StoreController storeController = new StoreController();
        
        private void LoadStoreList()
        {
            List<Store> stores = storeController.GetAll();
            this.dtgvStore.DataSource = stores;
        }
        public ListStore()
        {
            InitializeComponent();
            LoadStoreList();
            txtMaKho.DataBindings.Add(new Binding("Text", dtgvStore.DataSource, "id", true, DataSourceUpdateMode.Never));
            this.txtTenKho.DataBindings.Add(new Binding("Text", dtgvStore.DataSource, "name", true, DataSourceUpdateMode.Never));
            this.txtDienTich.DataBindings.Add(new Binding("Text", dtgvStore.DataSource, "area", true));
            this.txtDiaChi.DataBindings.Add(new Binding("Text", dtgvStore.DataSource, "storeAddress", true));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
            string name =  this.txtTenKho.Text;
            float area = float.Parse(this.txtDienTich.Text);
            string address = this.txtDiaChi.Text;
            Store store = new Store(name,area,address);
            try
            {
                if (this.storeController.Add(store))
                {
                    LoadStoreList();
                    MessageBox.Show("Đã thêm kho");
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Thêm kho thất bại");
            }
              
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtMaKho.Text);
            try
            {
                if (this.storeController.Delete(id))
                {
                    LoadStoreList();
                    MessageBox.Show("Xoá kho thành công");
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtMaKho.Text);
            string name = this.txtTenKho.Text;
            float area = float.Parse(this.txtDienTich.Text);
            string address = this.txtDiaChi.Text;
            Store store = new Store(id,name, area, address);
            try
            {
                if (this.storeController.Edit(store))
                {
                    LoadStoreList();
                    MessageBox.Show("Sửa kho thành công");
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
