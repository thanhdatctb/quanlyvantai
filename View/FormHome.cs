﻿using Microsoft.SqlServer.Management.Sdk.Sfc;
using QuanLyVanTai.Model;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace QuanLyVanTai.View
{
    public partial class FormHome : Form
    {
        public FormHome()
        {
            InitializeComponent();
        }
        private Form activeForm = null;
        private void ExportToExcel(string query, string fileName)
        {
            DataTable dataTable = MyConnection.ExecuteQuery(query);
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;

                //Create a new workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];

                //Create a dataset from XML file


                //Import data from the data table with column header, at first row and first column, 
                //and by its column type.
                sheet.ImportDataTable(dataTable, true, 1, 1, true);

                //Creating Excel table or list object and apply style to the table
                IListObject table = sheet.ListObjects.Create("Employee_PersonalDetails", sheet.UsedRange);

                table.BuiltInTableStyle = TableBuiltInStyles.TableStyleMedium14;

                //Autofit the columns
                sheet.UsedRange.AutofitColumns();

                //Save the file in the given path
                String path = "../../excel/" + fileName + ".xlsx";
                string fullPath = Path.GetFullPath(path);
                Stream excelStream = File.Create(fullPath);
                workbook.SaveAs(excelStream);
                excelStream.Dispose();
                System.Windows.Forms.MessageBox.Show("Đã xuất ra file " + fullPath);
            }
        }
        private void openWorkForm(Form workForm)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = workForm;
            workForm.TopLevel = false;
            workForm.FormBorderStyle = FormBorderStyle.None;
            workForm.Dock = DockStyle.Fill;
            panelWorkMain.Controls.Add(workForm);
            panelWorkMain.Tag = workForm;
            workForm.BringToFront();
            workForm.Show();
        }
        private void MenuToolTripDX_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MenuToolTripNCC_Click(object sender, EventArgs e)
        {
            openWorkForm(new ListStore());
        }

        private void MenuToolTripDK_Click(object sender, EventArgs e)
        {
            new FormSignUp().Show();

        }

        private void MenuToolTripVT_Click(object sender, EventArgs e)
        {
            openWorkForm(new ListCustomer());
        }

        private void MenuToolTripNK_Click(object sender, EventArgs e)
        {
            openWorkForm(new ImportForm());
        }

        private void thôngTinVậnChuyểnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openWorkForm(new FormShipInfomation());
        }

        private void MenuToolTripRevenue_Click(object sender, EventArgs e)
        {
            new FormRevenue().Show();
        }

        private void DT_Click(object sender, EventArgs e)
        {
            string sql = "select receiveDate as [Ngày], SUM(weigh* unitPrice * distance) as [Tổng doanh thu] from orders group by receiveDate";
            this.ExportToExcel(sql, "Doanh thu");
        }
    }
}
