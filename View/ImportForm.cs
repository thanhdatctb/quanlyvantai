﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class ImportForm : Form
    {
        private CustomerController customerController = new CustomerController();
        private StoreController storeController = new StoreController();
        private OrderController orderController = new OrderController();
        private bool customerExist = false;
        public ImportForm()
        {
            InitializeComponent();
            this.cbStore.DataSource = storeController.GetAll();
            this.cbStore.DisplayMember = "name";
            this.cbStore.ValueMember = "id";
        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {
            string phone = this.txtPhone.Text;
            Customer customer = this.customerController.GetByPhone(phone);
            if(customer == null)
            {
                this.txtName.Enabled = true;
                this.txtAddress.Enabled = true;
                this.txtCustomerId.Text = "";
                customerExist = false;
            }    
            else
            {
                this.txtName.Enabled = true;
                this.txtAddress.Enabled = true;
                this.txtCustomerId.Text = customer.id.ToString();
                this.txtName.Text = customer.name;
                this.txtAddress.Text = customer.address;
                this.txtName.Enabled = false;
                this.txtAddress.Enabled = false;
                customerExist = true;
            }    
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this.cbStore.SelectedValue.ToString());
            string name = this.txtName.Text;
            string address = this.txtAddress.Text;
            string phone = this.txtPhone.Text;
            Customer customer = new Customer(name, address, phone);
            if (customerExist)
            {
                this.customerController.Add(customer);
            }
            int customerId = this.customerController.GetByPhone(phone).id;
            int storeId = (int) this.cbStore.SelectedValue;
            string commodityName = this.txtComodityName.Text;
            float weigh = float.Parse(this.txtWeight.Text);
            DateTime receiveDate = this.dpDReceiveDate.Value;
            DateTime shipDate = this.dpShipDate.Value;
            string receiveAddress = this.txtShipAddress.Text;
            float unitPrice = float.Parse(this.txtUnitPrice.Text);
            float distance = float.Parse(this.txtDistance.Text);
            string status = "Chưa hoàn thành";
            Order order = new Order(customerId, storeId, commodityName, weigh, receiveDate, shipDate, receiveAddress, status, unitPrice, distance);
            try
            {
                if (orderController.Add(order))
                {
                    MessageBox.Show("Thêm đơn hàng thành công");
                }    
                    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}
