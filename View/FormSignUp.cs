﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class FormSignUp : Form
    {
        private AdminController adminController = new AdminController();
        public FormSignUp()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            this.Close();
            new LogIn().Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            string username = this.txtUserName.Text;
            string hashpass = PasswordController.Sha1Hash(this.txtPassword.Text);
            string hashconfirm = PasswordController.Sha1Hash(this.txtConfirm.Text);
            
            if(hashpass!=hashconfirm)
            {
                MessageBox.Show("Mật khẩu không khớp");
                this.Close();
                return;
            }   
            try
            {
                Admin ad = new Admin(username, hashpass);
                adminController.Add(ad);
                MessageBox.Show("Đăng ký thành công");
            }catch(Exception ex)
            {
                MessageBox.Show("Đăng ký thất bại");
            }
            
        }
    }
}
