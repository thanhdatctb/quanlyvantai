﻿using QuanLyVanTai.Controller;
using QuanLyVanTai.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyVanTai.View
{
    public partial class ListCustomer : Form
    {
        CustomerController customerController = new CustomerController();
        public ListCustomer()
        {
            InitializeComponent();
            LoadData();
            this.txtCustomerId.DataBindings.Add(new Binding("Text", this.dtgvCustomer.DataSource, "id"));
            this.txtName.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "name"));
            this.txtAddress.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "address"));
            this.txtPhone.DataBindings.Add(new Binding("Text", dtgvCustomer.DataSource, "phone"));

        }
        private void LoadData()
        {
            this.dtgvCustomer.DataSource = this.customerController.GetAll();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtCustomerId.Text);
            string name = this.txtName.Text;
            string address = this.txtAddress.Text;
            string phone = this.txtPhone.Text;
            Customer customer = new Customer(id, name, address, phone);
            try
            {
               if(this.customerController.Add(customer))
               {
                    LoadData();
                    MessageBox.Show("Thêm khách hàng thành công");
               }    
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtCustomerId.Text);
            try
            {
                if (this.customerController.Delete(id))
                {
                    LoadData();
                    MessageBox.Show("Xóa khách hàng thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtCustomerId.Text);
            string name = this.txtName.Text;
            string address = this.txtAddress.Text;
            string phone = this.txtPhone.Text;
            Customer customer = new Customer(id, name, address, phone);
            try
            {
                if (this.customerController.Edit(customer))
                {
                    LoadData();
                    MessageBox.Show("Sửa khách hàng thành công");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
