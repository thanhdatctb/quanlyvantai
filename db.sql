﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyVanTai'))
drop database QuanLyVanTai
go
create database QuanLyVanTai
go
use QuanLyVanTai
go
create table admin
(
	id int primary key identity,
	usename varchar(100),
	hashpass varchar(100)
)
go
create table store
(
	id int primary key identity (1,1),
	name nvarchar(100),
	area float,
	Storeaddress nvarchar(100)
)
go
create table customer
(
	id int primary key identity,
	name nvarchar(100),
	address nvarchar(300),
	phone varchar(11)
)
go
create table orders
(
	id int primary key identity(1,1),
	CustomerId int foreign key references Customer(id),
	StoreID int foreign key references store(id),
	commodityName nvarchar(100),
	weigh float,
	receiveDate datetime,
	shipDate datetime,
	shipAddress nvarchar(100),
	status nvarchar(100),
	unitPrice float,
	distance float
)
go
create table shipstatus
(
	id int primary key identity,
	name nvarchar(100)
)
go
insert into shipstatus (name) values
(N'Hoàn thành'),
(N'Chưa hoàn thành')
go 
insert into orders (CustomerId,StoreID,commodityName,weigh,receiveDate,shipDate,shipAddress,status,unitPrice,distance) values
						(1,2,N'Bông',1.5,'12-12-2012','12-13-2012',N'Hà Nam',N'Chưa hoàn thành',200.5,40.8)
go
select * from orders
go
---username: admin, password: admin, hash: sha1
insert into admin (usename,hashpass)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997')
go 
insert into store (name, area, Storeaddress) values
(N'Hòa Phát', 1000, N'Hà Nội'),
(N'Hoa Sen', 800, N'Hà Nam'),
(N'Faros',700,'Đà Nẵng')
go
insert into customer (name, address, phone) values
(N'Nguyễn Văn A', 'Hà Nội','0123456789'),
(N'Trần Văn B', 'Thái Bình', '0912345678')
select * from admin
select receiveDate, SUM(weigh* unitPrice * distance) as total from orders group by receiveDate